public class Factorial {
	
	public static void main (String[] args) {
		System.out.println ("Hello, BU!");
		System.out.println (fact(-1));
	}
		
	/* Recursive implementation of factorial */
	public static int fact (int n) {
		if (n < 0) // Positive integers only
			return n;
		if (n == 0) // Base Case
			return 1;
		return (n * fact(n-1)); // Recursive Case
	}

}