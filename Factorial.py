# Recursive implementation of factorial
def fact(n):
	if (n < 0):  ## Positive integers only
		return n
	if (n == 0): ## Base Case
		return 1
	return (n * fact(n-1)) ## Recursive Case